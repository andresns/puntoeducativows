USE [master]
GO
/****** Object:  Database [PuntoEducativoWS]    Script Date: 23-06-2019 12:09:36 ******/
CREATE DATABASE [PuntoEducativoWS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PuntoEducativoWS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\PuntoEducativoWS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PuntoEducativoWS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\PuntoEducativoWS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [PuntoEducativoWS] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PuntoEducativoWS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PuntoEducativoWS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET ARITHABORT OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PuntoEducativoWS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PuntoEducativoWS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PuntoEducativoWS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PuntoEducativoWS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET RECOVERY FULL 
GO
ALTER DATABASE [PuntoEducativoWS] SET  MULTI_USER 
GO
ALTER DATABASE [PuntoEducativoWS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PuntoEducativoWS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PuntoEducativoWS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PuntoEducativoWS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PuntoEducativoWS] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PuntoEducativoWS', N'ON'
GO
ALTER DATABASE [PuntoEducativoWS] SET QUERY_STORE = OFF
GO
USE [PuntoEducativoWS]
GO
/****** Object:  Table [dbo].[cabecera_cotizacion]    Script Date: 23-06-2019 12:09:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cabecera_cotizacion](
	[codigo_cotizacion] [varchar](20) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[run_proveedor] [varchar](12) NOT NULL,
 CONSTRAINT [PK_cabecera_cotizacion] PRIMARY KEY CLUSTERED 
(
	[codigo_cotizacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cabecera_orden_compra]    Script Date: 23-06-2019 12:09:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cabecera_orden_compra](
	[codigo_orden_compra] [varchar](20) NOT NULL,
	[run_cliente] [varchar](12) NOT NULL,
	[nombre_cliente] [varchar](200) NOT NULL,
	[direccion_cliente] [varchar](250) NOT NULL,
	[fono_cliente] [int] NULL,
	[email_cliente] [varchar](200) NOT NULL,
	[fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_cabecera_orden_compra] PRIMARY KEY CLUSTERED 
(
	[codigo_orden_compra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detalle_cotizacion]    Script Date: 23-06-2019 12:09:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalle_cotizacion](
	[cantidad] [int] NOT NULL,
	[precio_unitario] [int] NOT NULL,
	[id_producto] [int] NOT NULL,
	[codigo_cotizacion] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detalle_orden_compra]    Script Date: 23-06-2019 12:09:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalle_orden_compra](
	[cantidad] [int] NOT NULL,
	[precio_unitario] [int] NOT NULL,
	[id_producto] [int] NOT NULL,
	[codigo_orden_compra] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[producto]    Script Date: 23-06-2019 12:09:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[producto](
	[id_producto] [int] NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
 CONSTRAINT [PK_producto] PRIMARY KEY CLUSTERED 
(
	[id_producto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[proveedor]    Script Date: 23-06-2019 12:09:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[proveedor](
	[run_proveedor] [varchar](12) NOT NULL,
	[nombre_proveedor] [varchar](200) NOT NULL,
	[direccion_proveedor] [varchar](250) NOT NULL,
	[fono_proveedor] [int] NULL,
	[email_proveedor] [varchar](200) NOT NULL,
 CONSTRAINT [PK_proveedor] PRIMARY KEY CLUSTERED 
(
	[run_proveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [PuntoEducativoWS] SET  READ_WRITE 
GO
