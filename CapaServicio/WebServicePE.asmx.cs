﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CapaConexion;
using CapaDTO;
using CapaNegocio;


namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServicePE
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/", Description = "Web Service de Punto Educativo")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServicePE : System.Web.Services.WebService
    {
        #region Producto

        [WebMethod(Description = "Obtiene los productos de la base de datos.")]
        public List<Producto> obtenerProductos()
        {
            NegocioProducto auxNegocioProducto = new NegocioProducto();

            List<Producto> auxListaProductos = new List<Producto>();

            auxListaProductos = auxNegocioProducto.obtenerProductos();

            return auxListaProductos;
        }

        [WebMethod(Description = "Ingresa un producto a la base de datos.")]
        public void ingresarProducto(Producto producto)
        {
            NegocioProducto auxNegocioProducto = new NegocioProducto();
            auxNegocioProducto.ingresarProducto(producto);
        }

        [WebMethod(Description = "Elimina un producto en base al ID.")]
        public void eliminarProducto(int idProducto)
        {
            NegocioProducto auxNegocioProducto = new NegocioProducto();

            auxNegocioProducto.eliminarProducto(idProducto);
        }

        [WebMethod(Description = "Modifica un producto.")]
        public void modificarProducto(Producto producto)
        {
            NegocioProducto auxNegocioProducto = new NegocioProducto();

            auxNegocioProducto.modificarProducto(producto);
        }

        [WebMethod(Description = "Busca un producto por Id.")]
        public Producto buscarProductoId(int idProducto)
        {
            NegocioProducto auxNegocioProducto = new NegocioProducto();
            Producto auxProducto = new Producto();

            auxProducto = auxNegocioProducto.buscarProductoId(idProducto);

            return auxProducto;
        }

        [WebMethod(Description = "Busca un producto por descripción.")]
        public List<Producto> buscarProductoDescripcion(string descripcion)
        {
            NegocioProducto auxNegocioProducto = new NegocioProducto();
            List<Producto> auxListaProductos = new List<Producto>();

            auxListaProductos = auxNegocioProducto.buscarProductoDescripcion(descripcion);

            return auxListaProductos;
        }

        #endregion Producto

        #region CabeceraCotizacion

        [WebMethod(Description = "Obtiene las CabeceraCotizacion de la base de datos.")]
        public List<CabeceraCotizacion> obtenerCabeceraCotizaciones()
        {
            NegocioCabeceraCotizacion auxNegocioCabeceraCotizacion = new NegocioCabeceraCotizacion();

            List<CabeceraCotizacion> auxListaCabeceraCotizaciones = new List<CabeceraCotizacion>();

            auxListaCabeceraCotizaciones = auxNegocioCabeceraCotizacion.obtenerCabeceraCotizaciones();

            return auxListaCabeceraCotizaciones;
        }

        [WebMethod(Description = "Obtiene las CabeceraCotizacion de la base de datos en base al codigo del proveedor.")]
        public List<CabeceraCotizacion> obtenerCabeceraCotizacionesProveedor(string runProveedor)
        {
            NegocioCabeceraCotizacion auxNegocioCabeceraCotizacion = new NegocioCabeceraCotizacion();

            List<CabeceraCotizacion> auxListaCabeceraCotizaciones = new List<CabeceraCotizacion>();

            auxListaCabeceraCotizaciones = auxNegocioCabeceraCotizacion.obtenerCabeceraCotizacionesProveedor(runProveedor);

            return auxListaCabeceraCotizaciones;
        }

        [WebMethod(Description = "Ingresa una CabeceraCotizacion a la base de datos.")]
        public void ingresarCabeceraCotizacion(CabeceraCotizacion cabeceraCotizacion)
        {
            NegocioCabeceraCotizacion auxNegocioCabeceraCotizacion = new NegocioCabeceraCotizacion();
            auxNegocioCabeceraCotizacion.ingresarCabeceraCotizacion(cabeceraCotizacion);
        }

        [WebMethod(Description = "Elimina una CabeceraCotizacion en base al codigo de la cotizacion.")]
        public void eliminarCabeceraCotizacion(string codigoCotizacion)
        {
            NegocioCabeceraCotizacion auxNegocioCabeceraCotizacion = new NegocioCabeceraCotizacion();

            auxNegocioCabeceraCotizacion.eliminarCabeceraCotizacion(codigoCotizacion);
        }

        #endregion CabeceraCotizacion

        #region Proveedor

        [WebMethod(Description = "Obtiene los Proveedores de la base de datos.")]
        public List<Proveedor> obtenerProveedores()
        {
            NegocioProveedor auxNegocioProveedor = new NegocioProveedor();

            List<Proveedor> auxListaProveedores = new List<Proveedor>();

            auxListaProveedores = auxNegocioProveedor.obtenerProveedores();

            return auxListaProveedores;
        }

        [WebMethod(Description = "Obtiene un Proveedor en base al rut.")]
        public Proveedor obtenerProveedorRut(string rutProveedor)
        {
            NegocioProveedor auxNegocioProveedor = new NegocioProveedor();

            Proveedor auxProveedor = new Proveedor();

            auxProveedor = auxNegocioProveedor.obtenerProveedorRut(rutProveedor);

            return auxProveedor;
        }

        [WebMethod(Description = "Ingresa un Proveedor a la base de datos.")]
        public void ingresarProveedor(Proveedor proveedor)
        {
            NegocioProveedor auxNegocioProveedor = new NegocioProveedor();
            auxNegocioProveedor.ingresarProveedor(proveedor);
        }

        [WebMethod(Description = "Elimina un Proveedor en base al run.")]
        public void eliminarProveedor(string runProveedor)
        {
            NegocioProveedor auxNegocioProveedor = new NegocioProveedor();

            auxNegocioProveedor.eliminarProveedor(runProveedor);
        }

        [WebMethod(Description = "Modifica un Proveedor.")]
        public void modificarProveedor(Proveedor proveedor)
        {
            NegocioProveedor auxNegocioProveedor = new NegocioProveedor();

            auxNegocioProveedor.modificarProveedor(proveedor);
        }

        #endregion Proveedor

        #region CabeceraOrdenCompra

        [WebMethod(Description = "Obtiene las cabeceras de las orden de compra de la base de datos.")]
        public List<CabeceraOrdenCompra> obtenerCabeceraOrdenCompras()
        {
            NegocioCabeceraOrdenCompra auxNegocioCabeceraOrdenCompra = new NegocioCabeceraOrdenCompra();

            List<CabeceraOrdenCompra> auxListaCabeceraOrdenCompras = new List<CabeceraOrdenCompra>();

            auxListaCabeceraOrdenCompras = auxNegocioCabeceraOrdenCompra.obtenerCabeceraOrdenCompras();

            return auxListaCabeceraOrdenCompras;
        }

        [WebMethod(Description = "Ingresa una CabeceraOrdenCompra a la base de datos.")]
        public void ingresarCabeceraOrdenCompra(CabeceraOrdenCompra cabeceraOrdenCompra)
        {
            NegocioCabeceraOrdenCompra auxCabeceraOrdenCompra = new NegocioCabeceraOrdenCompra();
            auxCabeceraOrdenCompra.ingresarCabeceraOrdenCompra(cabeceraOrdenCompra);
        }

        [WebMethod(Description = "Elimina una CabeceraOrdenCompra en base al codigo de la orden de compra.")]
        public void eliminarCabeceraOrdenCompra(string codigoOrdenCompra)
        {
            NegocioCabeceraOrdenCompra auxNegocioCabeceraOrdenCompra = new NegocioCabeceraOrdenCompra();

            auxNegocioCabeceraOrdenCompra.eliminarCabeceraOrdenCompra(codigoOrdenCompra);
        }

        #endregion CabeceraOrdenCompra

        #region DetalleOrdenCompra

        [WebMethod(Description = "Obtiene los detalles de las orden de compra de la base de datos.")]
        public List<DetalleOrdenCompra> obtenerDetalleOrdenCompras()
        {
            NegocioDetalleOrdenCompra auxNegocioDetalleOrdenCompra = new NegocioDetalleOrdenCompra();

            List<DetalleOrdenCompra> auxListaDetalleOrdenCompras = new List<DetalleOrdenCompra>();

            auxListaDetalleOrdenCompras = auxNegocioDetalleOrdenCompra.obtenerDetalleOrdenCompras();

            return auxListaDetalleOrdenCompras;
        }

        [WebMethod(Description = "Ingresa un DetalleOrdenCompra a la base de datos.")]
        public void ingresarDetalleOrdenCompra(DetalleOrdenCompra detalleOrdenCompra)
        {
            NegocioDetalleOrdenCompra auxDetalleOrdenCompra = new NegocioDetalleOrdenCompra();
            auxDetalleOrdenCompra.ingresarDetalleOrdenCompra(detalleOrdenCompra);
        }

        [WebMethod(Description = "Elimina un DetalleOrdenCompra en base al codigo de la orden de compra y el id del producto.")]
        public void eliminarDetalleOrdenCompra(int idProducto, string codigoOrdenCompra)
        {
            NegocioDetalleOrdenCompra auxNegocioDetalleOrdenCompra = new NegocioDetalleOrdenCompra();

            auxNegocioDetalleOrdenCompra.eliminarDetalleOrdenCompra(idProducto, codigoOrdenCompra);
        }

        #endregion DetalleOrdenCompra

        #region DetalleCotizacion

        [WebMethod(Description = "Obtiene los detalles de las cotizaciones de la base de datos.")]
        public List<DetalleCotizacion> obtenerDetalleCotizaciones()
        {
            NegocioDetalleCotizacion auxNegocioDetalleCotizacion = new NegocioDetalleCotizacion();

            List<DetalleCotizacion> auxListaDetalleCotizaciones = new List<DetalleCotizacion>();

            auxListaDetalleCotizaciones = auxNegocioDetalleCotizacion.obtenerDetalleCotizaciones();

            return auxListaDetalleCotizaciones;
        }

        [WebMethod(Description = "Obtiene los detalles de las cotizaciones de la base de datos en base al codigo de la cotizacion.")]
        public List<DetalleCotizacion> obtenerDetalleCotizacionCodigo(string codigoCotizacion)
        {
            NegocioDetalleCotizacion auxNegocioDetalleCotizacion = new NegocioDetalleCotizacion();

            List<DetalleCotizacion> auxListaDetalleCotizaciones = new List<DetalleCotizacion>();

            auxListaDetalleCotizaciones = auxNegocioDetalleCotizacion.buscarDetalleCotizacionCodigo(codigoCotizacion);

            return auxListaDetalleCotizaciones;
        }

        [WebMethod(Description = "Ingresa un DetalleCotizacion a la base de datos.")]
        public void ingresarDetalleCotizacion(DetalleCotizacion detalleCotizacion)
        {
            NegocioDetalleCotizacion auxDetalleCotizacion = new NegocioDetalleCotizacion();
            auxDetalleCotizacion.ingresarDetalleCotizacion(detalleCotizacion);
        }

        [WebMethod(Description = "Modifica la cantidad y precio de un DetalleCotizacion.")]
        public void modificarDetalleCotizacion(DetalleCotizacion detalleCotizacion)
        {
            NegocioDetalleCotizacion auxNegocioDetalleCotizacion = new NegocioDetalleCotizacion();

            auxNegocioDetalleCotizacion.modificarDetalleCotizacion(detalleCotizacion);
        }

        [WebMethod(Description = "Elimina un DetalleCotizacion en base al codigo de la cotizacion y el id del producto.")]
        public void eliminarDetalleCotizacion(int idProducto, string codigoCotizacion)
        {
            NegocioDetalleCotizacion auxNegocioDetalleCotizacion = new NegocioDetalleCotizacion();

            auxNegocioDetalleCotizacion.eliminarDetalleCotizacion(idProducto, codigoCotizacion);
        }

        [WebMethod(Description = "Elimina todos los DetalleCotizacion asociados a un codigo de cotizacion.")]
        public void eliminarDetalleCotizacionCodigo(string codigoCotizacion)
        {
            NegocioDetalleCotizacion auxNegocioDetalleCotizacion = new NegocioDetalleCotizacion();

            auxNegocioDetalleCotizacion.eliminarDetalleCotizacionCodigo(codigoCotizacion);
        }

        #endregion DetalleCotizacion

        #region ConsumoAPIMercadoPublico

        [WebMethod(Description = "Ingresa a la base de datos las cabeceras y detalles de las ordenes de compra en base a una fecha determinada.")]
        public void ingresarOrdenesCompraFecha(DateTime fecha)
        {
            NegocioOrdenCompra auxNegocioOrdenCompra = new NegocioOrdenCompra();
            List<String> auxListaCodigosOrdenCompra = new List<string>();

            auxListaCodigosOrdenCompra = auxNegocioOrdenCompra.obtenerOrdenesCompraFecha(fecha);

            List<OrdenCompra> auxListaOrdenCompra = new List<OrdenCompra>();
            foreach(string codigoOC in auxListaCodigosOrdenCompra)
            {
                OrdenCompra auxOrdenCompra = new OrdenCompra();
                auxOrdenCompra = auxNegocioOrdenCompra.obtenerOrdenCompraCodigo(codigoOC);
                auxListaOrdenCompra.Add(auxOrdenCompra);
            }
            
            foreach(OrdenCompra oc in auxListaOrdenCompra)
            {
                NegocioCabeceraOrdenCompra auxNegocioCabeceraOrdenCompra = new NegocioCabeceraOrdenCompra();
                auxNegocioCabeceraOrdenCompra.ingresarCabeceraOrdenCompra(oc.CabeceraOrdenCompra);

                NegocioDetalleOrdenCompra auxNegocioDetalleOrdenCompra = new NegocioDetalleOrdenCompra();

                foreach (DetalleOrdenCompra doc in oc.DetalleOrdenCompra)
                {
                    auxNegocioDetalleOrdenCompra.ingresarDetalleOrdenCompra(doc);
                }

            }
            
        }

        #endregion ConsumoAPIMercadoPublico
    }
}
