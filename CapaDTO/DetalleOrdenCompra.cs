﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class DetalleOrdenCompra
    {
        private int cantidad;
        private int precioUnitario;
        private int idProducto;
        private string codigoOrdenCompra;

        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int PrecioUnitario { get => precioUnitario; set => precioUnitario = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }
        public string CodigoOrdenCompra { get => codigoOrdenCompra; set => codigoOrdenCompra = value; }
    }
}
