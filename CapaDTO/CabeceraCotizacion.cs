﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class CabeceraCotizacion
    {
        private string codigoCotizacion;
        private DateTime fecha;
        private string runProveedor;

        public string CodigoCotizacion { get => codigoCotizacion; set => codigoCotizacion = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string RunProveedor { get => runProveedor; set => runProveedor = value; }
    }
}
