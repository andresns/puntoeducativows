﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class OrdenCompra
    {
        private CabeceraOrdenCompra cabeceraOrdenCompra;
        private List<DetalleOrdenCompra> detalleOrdenCompra;

        public CabeceraOrdenCompra CabeceraOrdenCompra { get => cabeceraOrdenCompra; set => cabeceraOrdenCompra = value; }
        public List<DetalleOrdenCompra> DetalleOrdenCompra { get => detalleOrdenCompra; set => detalleOrdenCompra = value; }
    }
}
