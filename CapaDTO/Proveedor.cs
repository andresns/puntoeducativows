﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Proveedor
    {
        private string runProveedor;
        private string nombreProveedor;
        private string direccionProveedor;
        private string fonoProveedor;
        private string emailProveedor;

        public string RunProveedor { get => runProveedor; set => runProveedor = value; }
        public string NombreProveedor { get => nombreProveedor; set => nombreProveedor = value; }
        public string DireccionProveedor { get => direccionProveedor; set => direccionProveedor = value; }
        public string FonoProveedor { get => fonoProveedor; set => fonoProveedor = value; }
        public string EmailProveedor { get => emailProveedor; set => emailProveedor = value; }


    }
}
