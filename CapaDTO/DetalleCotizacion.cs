﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class DetalleCotizacion
    {
        private int cantidad;
        private double precioUnitario;
        private int idProducto;
        private string codCotizacion;

        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double PrecioUnitario { get => precioUnitario; set => precioUnitario = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }
        public string CodCotizacion { get => codCotizacion; set => codCotizacion = value; }
    }
}
