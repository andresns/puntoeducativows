﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class CabeceraOrdenCompra
    {
        private string codigoOrdenCompra;
        private string runCliente;
        private string nombreCliente;
        private string direccionCliente;
        private string fonoCliente;
        private string emailCliente;
        private DateTime fecha;

        public string CodigoOrdenCompra { get => codigoOrdenCompra; set => codigoOrdenCompra = value; }
        public string RunCliente { get => runCliente; set => runCliente = value; }
        public string NombreCliente { get => nombreCliente; set => nombreCliente = value; }
        public string DireccionCliente { get => direccionCliente; set => direccionCliente = value; }
        public string FonoCliente { get => fonoCliente; set => fonoCliente = value; }
        public string EmailCliente { get => emailCliente; set => emailCliente = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
    }
}
