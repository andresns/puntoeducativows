﻿using CapaDTO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioOrdenCompra
    {
        public List<String> obtenerOrdenesCompraFecha(DateTime fecha)
        {
            string fechaConsulta = fecha.ToString("ddMMyyyy");
            string urlConsuta = "http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?fecha=" + fechaConsulta + "&CodigoProveedor=1263978&ticket=F8537A18-6766-4DEF-9E59-426B4FEE2844";

            List<String> auxListaOrdenesCompra = new List<string>();

            using (WebClient wc = new WebClient())
            {
                var jsonString = wc.DownloadString(urlConsuta);

                JObject jObject = JObject.Parse(jsonString);

                JArray ordenesCompra = (JArray)jObject.SelectToken("Listado");
                foreach (JToken oc in ordenesCompra)
                {
                    string codigoOrdenCompra = (string)oc.SelectToken("Codigo");
                    auxListaOrdenesCompra.Add(codigoOrdenCompra);
                    
                }
            }

            return auxListaOrdenesCompra;
        }

        public OrdenCompra obtenerOrdenCompraCodigo(string codigoOrdenCompra)
        {
            OrdenCompra auxOrdenCompra = new OrdenCompra();

            
            using (WebClient wc = new WebClient())
            {
                string urlConsulta = "http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?codigo=" + codigoOrdenCompra + "&ticket=F8537A18-6766-4DEF-9E59-426B4FEE2844";
                var jsonString = wc.DownloadString(urlConsulta);

                JObject jObject = JObject.Parse(jsonString);

                CabeceraOrdenCompra auxCabeceraOrdenCompra = new CabeceraOrdenCompra();
                List<DetalleOrdenCompra> auxListaDetalleOrdenCompra = new List<DetalleOrdenCompra>();
                
                //Obtenemos Cabecera OC
                auxCabeceraOrdenCompra.CodigoOrdenCompra = (string)jObject.SelectToken("Listado[0].Codigo");
                auxCabeceraOrdenCompra.RunCliente = (string)jObject.SelectToken("Listado[0].Comprador.RutUnidad");
                auxCabeceraOrdenCompra.NombreCliente = (string)jObject.SelectToken("Listado[0].Comprador.NombreOrganismo");
                auxCabeceraOrdenCompra.DireccionCliente = (string)jObject.SelectToken("Listado[0].Comprador.DireccionUnidad") + ", " + (string)jObject.SelectToken("Listado[0].Comprador.ComunaUnidad");
                auxCabeceraOrdenCompra.FonoCliente = (string)jObject.SelectToken("Listado[0].Comprador.FonoContacto");

                auxCabeceraOrdenCompra.EmailCliente = (string)jObject.SelectToken("Listado[0].Comprador.MailContacto");

                string auxFechaOC = (string)jObject.SelectToken("FechaCreacion");
                auxCabeceraOrdenCompra.Fecha = Convert.ToDateTime(auxFechaOC);


                //Obtenemos Detalle OC
                JArray productos = (JArray)jObject.SelectToken("Listado[0].Items.Listado");

                foreach (JToken p in productos)
                {
                    string idProducto = (string)p.SelectToken("CodigoProducto");
                    string descripcion = (string)p.SelectToken("EspecificacionComprador");
                    int cantidad = (int)p.SelectToken("Cantidad");
                    int precioUnitario = (int)p.SelectToken("PrecioNeto");

                    //Ingresamos el producto a base de datos
                    NegocioProducto auxNegocioProducto = new NegocioProducto();
                    Producto auxProducto = new Producto();

                    auxProducto.Id_producto = int.Parse(idProducto);
                    auxProducto.Descripcion = descripcion;

                    auxNegocioProducto.ingresarProducto(auxProducto);

                    DetalleOrdenCompra auxDetalleOrdenCompra = new DetalleOrdenCompra();

                    auxDetalleOrdenCompra.Cantidad = cantidad;
                    auxDetalleOrdenCompra.PrecioUnitario = precioUnitario;
                    auxDetalleOrdenCompra.IdProducto = int.Parse(idProducto);
                    auxDetalleOrdenCompra.CodigoOrdenCompra = auxCabeceraOrdenCompra.CodigoOrdenCompra;

                    auxListaDetalleOrdenCompra.Add(auxDetalleOrdenCompra);

                }

                auxOrdenCompra.CabeceraOrdenCompra = auxCabeceraOrdenCompra;
                auxOrdenCompra.DetalleOrdenCompra = auxListaDetalleOrdenCompra;
            }

            return auxOrdenCompra;
        }
    }
}
