﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioDetalleOrdenCompra
    {
        private Conexion conec1;

        public Conexion Conec1
        {
            get { return conec1; }
            set { conec1 = value; }
        }

        public void configurarConexion()
        {
            this.Conec1 = new Conexion();
            this.Conec1.NombreBaseDeDatos = "PuntoEducativoWS";
            this.Conec1.NombreTabla = "detalle_orden_compra";
            this.Conec1.CadenaConexion = "Data Source=DESKTOP-I01RF7O;Initial Catalog=PuntoEducativoWS;Integrated Security=True";

        }

        public void ingresarDetalleOrdenCompra(DetalleOrdenCompra detalleOrdenCompra)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO detalle_orden_compra (cantidad,precio_unitario,id_producto,codigo_orden_compra) " +
                                     " VALUES (" + detalleOrdenCompra.Cantidad + "," +
                                      detalleOrdenCompra.PrecioUnitario + "," +
                                      detalleOrdenCompra.IdProducto + ",'" +
                                      detalleOrdenCompra.CodigoOrdenCompra + "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }

        //public void modificarProveedor(Proveedor proveedor)
        //{
        //    this.configurarConexion();
        //    this.Conec1.CadenaSQL = "UPDATE proveedor set " +
        //                            "nombre_proveedor = '" + proveedor.NombreProveedor + "'," +
        //                            "direccion_proveedor = '" + proveedor.DireccionProveedor + "'," +
        //                            "fono_proveedor = " + proveedor.FonoProveedor + "," +
        //                            "email_proveedor = '" + proveedor.EmailProveedor + "'" +
        //                             " WHERE run_proveedor = '" +
        //                             proveedor.RunProveedor + "';";
        //    this.Conec1.EsSelect = false;
        //    this.Conec1.conectar();

        //}

        public void eliminarDetalleOrdenCompra(int idProducto, string codigoOrdenCompra)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM detalle_orden_compra " +
                                    " WHERE id_producto = " + idProducto + " AND codigo_orden_compra like '" + codigoOrdenCompra + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public List<DetalleOrdenCompra> obtenerDetalleOrdenCompras()
        {
            List<DetalleOrdenCompra> auxListaDetalleOrdenCompras = new List<DetalleOrdenCompra>();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM detalle_orden_compra;";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
            {
                DetalleOrdenCompra auxDetalleOrdenCompra = new DetalleOrdenCompra();
                auxDetalleOrdenCompra.Cantidad = (int)dr["cantidad"];
                auxDetalleOrdenCompra.PrecioUnitario = (int)dr["precio_unitario"];
                auxDetalleOrdenCompra.IdProducto = (int)dr["id_producto"];
                auxDetalleOrdenCompra.CodigoOrdenCompra = (String)dr["codigo_orden_compra"];
                auxListaDetalleOrdenCompras.Add(auxDetalleOrdenCompra);
            } //Fin for


            return auxListaDetalleOrdenCompras;
        }

        //public List<Producto> buscarProductoDescripcion(string descripcion)
        //{
        //    List<Producto> auxListaProducto = new List<Producto>();
        //    this.configurarConexion();
        //    this.Conec1.CadenaSQL = "SELECT * FROM producto " +
        //                        " WHERE descripcion LIKE '%" + descripcion + "%';";
        //    this.Conec1.EsSelect = true;
        //    this.Conec1.conectar();

        //    foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
        //    {
        //        Producto auxProducto = new Producto();
        //        auxProducto.Id_producto = (int)dr["id_producto"];
        //        auxProducto.Descripcion = (String)dr["descripcion"];
        //        auxListaProducto.Add(auxProducto);
        //    } //Fin for


        //    return auxListaProducto;
        //}

        //public Producto buscarProductoId(int idProducto)
        //{
        //    Producto auxProducto = new Producto();
        //    this.configurarConexion();

        //    this.Conec1.CadenaSQL = "SELECT * FROM producto " +
        //                        " WHERE id_producto = " + idProducto + ";";

        //    this.Conec1.EsSelect = true;
        //    this.Conec1.conectar();
        //    DataTable dt = new DataTable();
        //    dt = this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla];

        //    try
        //    {
        //        auxProducto.Id_producto = (int)dt.Rows[0]["id_producto"];
        //        auxProducto.Descripcion = (String)dt.Rows[0]["descripcion"];
        //    }
        //    catch (Exception ex)
        //    {
        //        auxProducto.Id_producto = 0;
        //        auxProducto.Descripcion = String.Empty;
        //    }

        //    return auxProducto;
        //}
    }
}
