﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioProveedor
    {
        private Conexion conec1;

        public Conexion Conec1
        {
            get { return conec1; }
            set { conec1 = value; }
        }

        public void configurarConexion()
        {
            this.Conec1 = new Conexion();
            this.Conec1.NombreBaseDeDatos = "PuntoEducativoWS";
            this.Conec1.NombreTabla = "proveedor";
            this.Conec1.CadenaConexion = "Data Source=DESKTOP-I01RF7O;Initial Catalog=PuntoEducativoWS;Integrated Security=True";

        }

        public void ingresarProveedor(Proveedor proveedor)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO proveedor (run_proveedor,nombre_proveedor,direccion_proveedor,fono_proveedor,email_proveedor) " +
                                     " VALUES ('" + proveedor.RunProveedor + "','" +
                                      proveedor.NombreProveedor + "','" +
                                      proveedor.DireccionProveedor + "','" +
                                      proveedor.FonoProveedor + "','" +
                                      proveedor.EmailProveedor + "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }

        public void modificarProveedor(Proveedor proveedor)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE proveedor set " +
                                    "nombre_proveedor = '" + proveedor.NombreProveedor + "'," + 
                                    "direccion_proveedor = '" + proveedor.DireccionProveedor + "'," +
                                    "fono_proveedor = '" + proveedor.FonoProveedor + "'," +
                                    "email_proveedor = '" + proveedor.EmailProveedor + "'" +
                                     " WHERE run_proveedor = '" +
                                     proveedor.RunProveedor + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }

        public void eliminarProveedor(string runProveedor)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM proveedor " +
                                    " WHERE run_proveedor like '" + runProveedor + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public List<Proveedor> obtenerProveedores()
        {
            List<Proveedor> auxListaProveedores = new List<Proveedor>();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM proveedor;";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
            {
                Proveedor auxProveedor = new Proveedor();
                auxProveedor.RunProveedor = (String)dr["run_proveedor"];
                auxProveedor.NombreProveedor = (String)dr["nombre_proveedor"];
                auxProveedor.DireccionProveedor = (String)dr["direccion_proveedor"];
                auxProveedor.FonoProveedor = (String)dr["fono_proveedor"];
                auxProveedor.EmailProveedor = (String)dr["email_proveedor"];
                auxListaProveedores.Add(auxProveedor);
            } //Fin for


            return auxListaProveedores;
        }

        public Proveedor obtenerProveedorRut(string runProveedor)
        {
            Proveedor auxProveedor = new Proveedor();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM proveedor" +
                                    " WHERE run_proveedor LIKE '" + runProveedor + "';";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            DataTable dt = new DataTable();
            dt = this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla];

            try
            {
                auxProveedor.RunProveedor = (String)dt.Rows[0]["run_proveedor"];
                auxProveedor.NombreProveedor = (String)dt.Rows[0]["nombre_proveedor"];
                auxProveedor.DireccionProveedor = (String)dt.Rows[0]["direccion_proveedor"];
                auxProveedor.FonoProveedor = (String)dt.Rows[0]["fono_proveedor"];
                auxProveedor.EmailProveedor = (String)dt.Rows[0]["email_proveedor"];
            }
            catch (Exception ex)
            {
                auxProveedor.RunProveedor = string.Empty;
            }

            return auxProveedor;
        }

    }
}
