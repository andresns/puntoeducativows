﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioDetalleCotizacion
    {
        private Conexion conec1;

        public Conexion Conec1
        {
            get { return conec1; }
            set { conec1 = value; }
        }

        public void configurarConexion()
        {
            this.Conec1 = new Conexion();
            this.Conec1.NombreBaseDeDatos = "PuntoEducativoWS";
            this.Conec1.NombreTabla = "detalle_cotizacion";
            this.Conec1.CadenaConexion = "Data Source=DESKTOP-I01RF7O;Initial Catalog=PuntoEducativoWS;Integrated Security=True";

        }

        public void ingresarDetalleCotizacion(DetalleCotizacion detalleCotizacion)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO detalle_cotizacion (cantidad,precio_unitario,id_producto,codigo_cotizacion) " +
                                     " VALUES (" + detalleCotizacion.Cantidad + "," +
                                      detalleCotizacion.PrecioUnitario + "," +
                                      detalleCotizacion.IdProducto + ",'" +
                                      detalleCotizacion.CodCotizacion + "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }

        public void modificarDetalleCotizacion(DetalleCotizacion detalleCotizacion)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE detalle_cotizacion set " +
                                    "cantidad = " + detalleCotizacion.Cantidad + "," +
                                    "precio_unitario = " + detalleCotizacion.PrecioUnitario +
                                     " WHERE id_producto = " +
                                     detalleCotizacion.IdProducto + "" +
                                     " AND codigo_cotizacion LIKE '" + detalleCotizacion.CodCotizacion + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }

        public void eliminarDetalleCotizacion(int idProducto, string codigoCotizacion)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM detalle_cotizacion " +
                                    " WHERE id_producto = " + idProducto + " AND codigo_cotizacion like '" + codigoCotizacion + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public void eliminarDetalleCotizacionCodigo(string codigoCotizacion)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM detalle_cotizacion " +
                                    " WHERE codigo_cotizacion like '" + codigoCotizacion + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public List<DetalleCotizacion> obtenerDetalleCotizaciones()
        {
            List<DetalleCotizacion> auxListaDetalleCotizaciones = new List<DetalleCotizacion>();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM detalle_cotizacion;";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
            {
                DetalleCotizacion auxDetalleCotizacion = new DetalleCotizacion();
                auxDetalleCotizacion.Cantidad = (int)dr["cantidad"];
                auxDetalleCotizacion.PrecioUnitario = (int)dr["precio_unitario"];
                auxDetalleCotizacion.IdProducto = (int)dr["id_producto"];
                auxDetalleCotizacion.CodCotizacion = (String)dr["codigo_cotizacion"];
                auxListaDetalleCotizaciones.Add(auxDetalleCotizacion);
            } //Fin for


            return auxListaDetalleCotizaciones;
        }

        public List<DetalleCotizacion> buscarDetalleCotizacionCodigo(string codigoCotizacion)
        {
            List<DetalleCotizacion> auxListaDetalleCotizaciones = new List<DetalleCotizacion>();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM detalle_cotizacion " +
                                " WHERE codigo_cotizacion LIKE '" + codigoCotizacion+ "';";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
            {
                DetalleCotizacion auxDetalleCotizacion = new DetalleCotizacion();
                auxDetalleCotizacion.Cantidad = (int)dr["cantidad"];
                auxDetalleCotizacion.PrecioUnitario = (int)dr["precio_unitario"];
                auxDetalleCotizacion.IdProducto = (int)dr["id_producto"];
                auxDetalleCotizacion.CodCotizacion = (String)dr["codigo_cotizacion"];
                auxListaDetalleCotizaciones.Add(auxDetalleCotizacion);
            } //Fin for


            return auxListaDetalleCotizaciones;
        }

        //public Producto buscarProductoId(int idProducto)
        //{
        //    Producto auxProducto = new Producto();
        //    this.configurarConexion();

        //    this.Conec1.CadenaSQL = "SELECT * FROM producto " +
        //                        " WHERE id_producto = " + idProducto + ";";

        //    this.Conec1.EsSelect = true;
        //    this.Conec1.conectar();
        //    DataTable dt = new DataTable();
        //    dt = this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla];

        //    try
        //    {
        //        auxProducto.Id_producto = (int)dt.Rows[0]["id_producto"];
        //        auxProducto.Descripcion = (String)dt.Rows[0]["descripcion"];
        //    }
        //    catch (Exception ex)
        //    {
        //        auxProducto.Id_producto = 0;
        //        auxProducto.Descripcion = String.Empty;
        //    }

        //    return auxProducto;
        //}
    }
}
