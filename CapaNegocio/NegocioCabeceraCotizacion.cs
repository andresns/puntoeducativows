﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioCabeceraCotizacion
    {

        private Conexion conec1;

        public Conexion Conec1
        {
            get { return conec1; }
            set { conec1 = value; }
        }

        public void configurarConexion()
        {
            this.Conec1 = new Conexion();
            this.Conec1.NombreBaseDeDatos = "PuntoEducativoWS";
            this.Conec1.NombreTabla = "cabecera_cotizacion";
            this.Conec1.CadenaConexion = "Data Source=DESKTOP-I01RF7O;Initial Catalog=PuntoEducativoWS;Integrated Security=True";

        }

        public void ingresarCabeceraCotizacion(CabeceraCotizacion cabeceraCotizacion)
        {
            this.configurarConexion();
            string fechaSQLServer = cabeceraCotizacion.Fecha.ToString("yyyy-MM-dd HH:mm:ss.fff");
            this.Conec1.CadenaSQL = "INSERT INTO cabecera_cotizacion (codigo_cotizacion,fecha,run_proveedor) " +
                                     " VALUES ('" + cabeceraCotizacion.CodigoCotizacion + "','" +
                                      fechaSQLServer + "','" + cabeceraCotizacion.RunProveedor + "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }


        public void eliminarCabeceraCotizacion(string codigoCotizacion)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM cabecera_cotizacion " +
                                    " WHERE codigo_cotizacion like '" + codigoCotizacion + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public List<CabeceraCotizacion> obtenerCabeceraCotizaciones()
        {
            List<CabeceraCotizacion> auxListaCabeceraCotizacion = new List<CabeceraCotizacion>();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM cabecera_cotizacion;";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
            {
                CabeceraCotizacion auxCabeceraCotizacion = new CabeceraCotizacion();
                auxCabeceraCotizacion.CodigoCotizacion = (String)dr["codigo_cotizacion"];
                auxCabeceraCotizacion.Fecha = (DateTime)dr["fecha"];
                auxCabeceraCotizacion.RunProveedor = (String)dr["run_proveedor"];
                auxListaCabeceraCotizacion.Add(auxCabeceraCotizacion);
            } //Fin for


            return auxListaCabeceraCotizacion;
        }

        public List<CabeceraCotizacion> obtenerCabeceraCotizacionesProveedor(string runProveedor)
        {
            List<CabeceraCotizacion> auxListaCabeceraCotizacion = new List<CabeceraCotizacion>();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM cabecera_cotizacion" +
                                    " WHERE run_proveedor LIKE '" + runProveedor + "';";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
            {
                CabeceraCotizacion auxCabeceraCotizacion = new CabeceraCotizacion();
                auxCabeceraCotizacion.CodigoCotizacion = (String)dr["codigo_cotizacion"];
                auxCabeceraCotizacion.Fecha = (DateTime)dr["fecha"];
                auxCabeceraCotizacion.RunProveedor = (String)dr["run_proveedor"];
                auxListaCabeceraCotizacion.Add(auxCabeceraCotizacion);
            } //Fin for


            return auxListaCabeceraCotizacion;
        }
    }
}
