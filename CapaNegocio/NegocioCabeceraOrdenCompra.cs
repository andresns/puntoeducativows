﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioCabeceraOrdenCompra
    {
        private Conexion conec1;

        public Conexion Conec1
        {
            get { return conec1; }
            set { conec1 = value; }
        }

        public void configurarConexion()
        {
            this.Conec1 = new Conexion();
            this.Conec1.NombreBaseDeDatos = "PuntoEducativoWS";
            this.Conec1.NombreTabla = "cabecera_orden_compra";
            this.Conec1.CadenaConexion = "Data Source=DESKTOP-I01RF7O;Initial Catalog=PuntoEducativoWS;Integrated Security=True";

        }

        public void ingresarCabeceraOrdenCompra(CabeceraOrdenCompra cabeceraOrdenCompra)
        {
            this.configurarConexion();

            if(buscarDuplicado(cabeceraOrdenCompra.CodigoOrdenCompra).CodigoOrdenCompra == "")
            {
                this.Conec1.CadenaSQL = "INSERT INTO cabecera_orden_compra (codigo_orden_compra,run_cliente,nombre_cliente,direccion_cliente,fono_cliente,email_cliente,fecha) " +
                                     " VALUES ('" + cabeceraOrdenCompra.CodigoOrdenCompra + "','" +
                                      cabeceraOrdenCompra.RunCliente + "','" +
                                      cabeceraOrdenCompra.NombreCliente + "','" +
                                      cabeceraOrdenCompra.DireccionCliente + "','" +
                                      cabeceraOrdenCompra.FonoCliente + "','" +
                                      cabeceraOrdenCompra.EmailCliente + "','" +
                                      cabeceraOrdenCompra.Fecha + "');";
                this.Conec1.EsSelect = false;
                this.Conec1.conectar();
            }
            

        }

        //public void modificarProveedor(Proveedor proveedor)
        //{
        //    this.configurarConexion();
        //    this.Conec1.CadenaSQL = "UPDATE proveedor set " +
        //                            "nombre_proveedor = '" + proveedor.NombreProveedor + "'," +
        //                            "direccion_proveedor = '" + proveedor.DireccionProveedor + "'," +
        //                            "fono_proveedor = " + proveedor.FonoProveedor + "," +
        //                            "email_proveedor = '" + proveedor.EmailProveedor + "'" +
        //                             " WHERE run_proveedor = '" +
        //                             proveedor.RunProveedor + "';";
        //    this.Conec1.EsSelect = false;
        //    this.Conec1.conectar();

        //}

        public void eliminarCabeceraOrdenCompra(string codigoOrdenCompra)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM cabecera_orden_compra " +
                                    " WHERE codigo_orden_compra like '" + codigoOrdenCompra + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public List<CabeceraOrdenCompra> obtenerCabeceraOrdenCompras()
        {
            List<CabeceraOrdenCompra> auxListaCabeceraOrdenCompras = new List<CabeceraOrdenCompra>();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM cabecera_orden_compra;";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
            {
                CabeceraOrdenCompra auxCabeceraOrdenCompra = new CabeceraOrdenCompra();
                auxCabeceraOrdenCompra.CodigoOrdenCompra = (String)dr["codigo_orden_compra"];
                auxCabeceraOrdenCompra.RunCliente = (String)dr["run_cliente"];
                auxCabeceraOrdenCompra.NombreCliente = (String)dr["nombre_cliente"];
                auxCabeceraOrdenCompra.DireccionCliente = (String)dr["direccion_cliente"];
                auxCabeceraOrdenCompra.FonoCliente = (String)dr["fono_cliente"];
                auxCabeceraOrdenCompra.EmailCliente = (String)dr["email_cliente"];
                auxCabeceraOrdenCompra.Fecha = (DateTime)dr["fecha"];
                auxListaCabeceraOrdenCompras.Add(auxCabeceraOrdenCompra);
            } //Fin for


            return auxListaCabeceraOrdenCompras;
        }

        //public List<Producto> buscarProductoDescripcion(string descripcion)
        //{
        //    List<Producto> auxListaProducto = new List<Producto>();
        //    this.configurarConexion();
        //    this.Conec1.CadenaSQL = "SELECT * FROM producto " +
        //                        " WHERE descripcion LIKE '%" + descripcion + "%';";
        //    this.Conec1.EsSelect = true;
        //    this.Conec1.conectar();

        //    foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
        //    {
        //        Producto auxProducto = new Producto();
        //        auxProducto.Id_producto = (int)dr["id_producto"];
        //        auxProducto.Descripcion = (String)dr["descripcion"];
        //        auxListaProducto.Add(auxProducto);
        //    } //Fin for


        //    return auxListaProducto;
        //}

        public CabeceraOrdenCompra buscarDuplicado(string codigoOrdenCompra)
        {
            CabeceraOrdenCompra auxCabeceraOrdenCompra = new CabeceraOrdenCompra();
            this.configurarConexion();

            this.Conec1.CadenaSQL = "SELECT * FROM cabecera_orden_compra " +
                                " WHERE codigo_orden_compra like '" + codigoOrdenCompra + "';";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            DataTable dt = new DataTable();
            dt = this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla];

            try
            {
                auxCabeceraOrdenCompra.CodigoOrdenCompra = (string)dt.Rows[0]["codigo_orden_compra"];
            }
            catch (Exception ex)
            {
                auxCabeceraOrdenCompra.CodigoOrdenCompra = "";
            }

            return auxCabeceraOrdenCompra;
        }
    }
}
